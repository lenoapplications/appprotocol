package protocol.models.abstracts;

public abstract class NotificationMethodsIdent {
    private final String templateRequest = "%sRequest";
    private final String templateResponse = "%sResponse";

    protected String request(String methodIdent){
        return String.format("%s%s",methodIdent,templateRequest);
    }
    protected String response(String methodIdent){
        return String.format("%s%s",methodIdent,templateResponse);
    }
}
