package protocol.statics.class_notification_protocol;

public class PcControllerNotificationProtocol {
    public static final String CLASS_IDENT = "PcCntrIdent";

    public static final String SHOW_CURRENT_PC_SCREEN_RESPONSE = "ShwCrntPcScrnResp";
    public static final String SHOW_CURRENT_PC_SCREEN_REQUEST = "ShwCrntPcScrnReq";

}
