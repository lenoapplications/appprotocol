package protocol.statics.class_notification_protocol;

import protocol.models.abstracts.NotificationMethodsIdent;

public class ConnectionNotificationProtocol extends NotificationMethodsIdent {
    public static final String CLASS_IDENT ="connectionIdent";

    public static final String CHECK_IF_SERVER_ONLINE_REQUEST = "chckIfSrvOnlineReq";
    public static final String CHECK_IF_SERVER_ONLINE_RESPONSE = "chckIfSrvOnlineResp";

    public static final String CONNECT_TO_SERVER_REQUEST =  "cnnctSrvReq";
    public static final String CONNECT_TO_SERVER_RESPONSE = "cnnctSrvResp";

    public static final String DISCONNECT_FROM_SERVER_REQUEST = "discnnctFrmSrvReq";
    public static final String DISCONNECT_FROM_SERVER_RESPONSE = "discnnctFrmSrvResp";


}
