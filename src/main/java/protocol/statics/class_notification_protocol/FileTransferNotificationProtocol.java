package protocol.statics.class_notification_protocol;

public class FileTransferNotificationProtocol {
    public static final String CLASS_IDENT_PC = "FileTransferIdentPc";
    public static final String CLASS_IDENT_PHONE = "FileTransferIdentPhone";

    public final static String CONNECT_TO_PC_STORAGE_REQUEST = "connectPcStrgReq";
    public final static String CONNECT_TO_PC_STORAGE_RESPONSE = "connectPcStrgRes";

    public static final String PC_STORAGE_OPEN_FOLDER_REQUEST = "PcStrgOpnReq";
    public static final String PC_STORAGE_OPEN_FOLDER_RESPONSE = "PcStrgOpnRes";

    public static final String PC_STORAGE_CLOSE_FOLDER_REQUEST = "PcStrgClsReq";
    public static final String PC_STORAGE_CLOSE_FOLDER_RESPONSE = "PcStrgClsRes";

    public static final String PC_STORAGE_SHOW_FILE_PROPERTIES_REQUEST = "PcStrSFPReq";
    public static final String PC_STORAGE_SHOW_FILE_PROPERTIES_RESPONSE = "PcStrSFPReq";

    public static final String PC_STORAGE_SHOW_FILE_CONTENT_REQUEST = "PcStrShFCntReq";
    public static final String PC_STORAGE_SHOW_FILE_CONTENT_RESPONSE = "PcStrShFCntRes";

}
