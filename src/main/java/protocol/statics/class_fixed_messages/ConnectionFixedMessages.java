package protocol.statics.class_fixed_messages;

public class ConnectionFixedMessages {
    public static final String SERVER_ONLINE = "onln";
    public static final String CONNECT_REQUEST = "cnnctReq";
    public static final String CONNECTION_APPROVED ="cnnctApr";
}
