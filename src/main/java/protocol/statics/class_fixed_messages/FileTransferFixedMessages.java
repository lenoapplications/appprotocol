package protocol.statics.class_fixed_messages;

import java.nio.file.spi.FileTypeDetector;

public class FileTransferFixedMessages {
    public static final String CONNECT_PC_STORAGE = "CnncPcStrg";

    public static final String OPEN_FOLDER = "OpnFldr:%s";
    public static final int OPEN_FOLDER_BEGIN_INDEX = OPEN_FOLDER.indexOf(":") + 1;


    public static final String CLOSE_FOLDER = "ClsFldr:%s|CrrntFldr:%s";
    public static final String[] extractFoldersAndCurrentFolder(String closeFolder){
        String[] parsedResult = new String[2];
        parsedResult[0] = closeFolder.substring(closeFolder.indexOf(":")+1,closeFolder.indexOf("|"));
        parsedResult[1] = closeFolder.substring(closeFolder.lastIndexOf(":")+1);
        return parsedResult;
    }

    public static final String FOLDER_EMPTY = "fldrEpty";
}
